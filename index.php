<?php

// require __DIR__ . '/readerauth.php';
include_once("config.php");

?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>AIS</title>
  </head>
  <body>
    <div class="jumbotron" style="margin-top: 60px;
    background-image:url(./lukas-blazek-mcSDtbWXUZU-unsplash.jpg);
    background-repeat: no-repeat;
    background-position:center center;
    background-size: cover;
    color: #F8ECE0;">
      <a class="navbar-brand" href="/index.php"><img src="jimflogo.png" width="70" height="50" class="d-inline-block align-top img-fluid" alt="" loading="lazy"></a>
  <h1 class="display-4">JIMF AIS</h1>
  <p class="lead">A bespoke accounting information system.</p>
  <hr class="my-4">
  
  <a class="btn btn-primary btn-lg" href="/dayman-index.php" role="button">DayMan</a>
  <a class="btn btn-primary btn-lg" href="/pm_index.php" role="button">PostMan</a>

</div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>
</html>
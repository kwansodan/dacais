<?php 

include_once("config.php");
include_once("acfunctions.php");
require __DIR__ . '/readerauth.php';


$result = mysqli_query($mysqli, "SELECT coa.accountno AS accountno, coa.accountname AS accountname, coalesce(openbals.bal,0) AS openbal, coalesce(debits.debit,0) AS debit, coalesce(credits.credit,0) AS credit, coalesce(currenttrans.curr,0) AS current, COALESCE(openbals.bal,0) + coalesce(currenttrans.curr,0) AS closingbal   FROM 
(SELECT distinct (accountno), accountname FROM coa) coa
LEFT JOIN
(SELECT account,bal FROM accountbalances WHERE baldate = DATE_FORMAT(NOW(),'%Y-01-01') or baldate = DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL 1 YEAR),'%Y-12-31')) openbals
ON coa.accountno = openbals.account
LEFT JOIN
(SELECT acnumber, SUM(amount) AS debit  FROM gl WHERE (dateo BETWEEN DATE_FORMAT(NOW(),'%Y-01-01') AND DATE_FORMAT(NOW(),'%Y-12-31')) AND amount >=0 GROUP BY acnumber )debits
ON coa.accountno = debits.acnumber
LEFT JOIN
(SELECT acnumber, -1*SUM(amount) AS credit  FROM gl WHERE (dateo BETWEEN DATE_FORMAT(NOW(),'%Y-01-01') AND DATE_FORMAT(NOW(),'%Y-12-31')) AND amount <0 GROUP BY acnumber )credits
ON coa.accountno = credits.acnumber
LEFT JOIN
(SELECT acnumber, SUM(amount) AS curr  FROM gl where dateo BETWEEN DATE_FORMAT(NOW(),'%Y-01-01') AND DATE_FORMAT(NOW(),'%Y-12-31') GROUP BY acnumber )currenttrans
ON coa.accountno = currenttrans.acnumber
ORDER BY coa.accountno");
?>

<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<title>Trial Balance</title>
<?php 
include 'header.php';
?>
</head>
<body>





<div class="card card-body">
<h5 class="card-title">Trial Balance</h5>

<p class="card-text"><?php echo date('Y-m-d H:i:s');?></p>
</div>



<table class="table table-sm" >
<thead class="thead-dark">
<tr>
<th scope="col">Account #</th>
<th scope="col">Account name</th>
<th scope="col">Opening balance</th>
<th scope="col">Debit</th>
<th scope="col">Credit</th>
<th scope="col">Net difference </th>
<th scope="col">Closing balance</th>
</tr>
</thead>
<tbody id="myTable">
<?php 
while($res = mysqli_fetch_array($result)) { 

echo "<tr>";
echo "<td>".$res['accountno']."</td>";
echo "<td>".$res['accountname']."</td>";
echo "<td>".$res['openbal']."</td>";
echo "<td>".$res['debit']."</td>";
echo "<td>".$res['credit']."</td>";
echo "<td>".$res['current']."</td>";
echo "<td>".$res['closingbal']."</td>";
}
?>
</tbody>
</table>  


</div>
</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>
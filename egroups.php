<?php 
include_once("config.php");
include_once("acfunctions.php");
$egroups = mysqli_query($mysqli, "SELECT * FROM egroups");
$result1 = mysqli_query($mysqli, "SELECT * FROM coa");

?>

<?php
if(isset($_POST['submit'])) {
$gname = mysqli_real_escape_string($mysqli, $_POST['gname']);
$descrip = mysqli_real_escape_string($mysqli, $_POST['descrip']);
$gpurpose = mysqli_real_escape_string($mysqli, $_POST['gpurpose']);
$pkey = mysqli_real_escape_string($mysqli, $_POST['pkey']);
$nkey = mysqli_real_escape_string($mysqli, $_POST['nkey']);
$costcat = mysqli_real_escape_string($mysqli, $_POST['costcat']);

  $result = mysqli_query($mysqli, "INSERT INTO egroups(groupname, descrip, purpose, positivekey, negativekey, costcat) VALUES('$gname','$descrip','$gpurpose','$pkey','$nkey', '$costcat')");
  if (!$result) {
    printf("Error: %s\n", mysqli_error($mysqli));
    exit();

}

  else{
    echo "<script type='text/javascript'> document.location = 'egroups.php'; </script>";
    exit();
}
}
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Employee Groups</title>
      <?php 
   include 'header.php';
?>
  </head>
  <body>
      <div class="container">

<table class="table table-sm "> 
  <!-- <thead>
    <tr>
      <th scope="col"></th>
      <th scope="col"></th>
      <th scope="col"></th>
      <th scope="col"></th>
      <th scope="col"></th>
      <th scope="col"></th>
      <th scope="col"></th>
    </tr>
  </thead> -->
  <tbody>
    <tr>
      <td ><div class="col-md-4 mb-3"><input type="text" class="form-control" id="searchbox" placeholder="Search..."  onkeyup="myFunction()"></div></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      
      <td></td>
      <td></td>
      <td></td>
      <td><button type="button" data-toggle="modal" data-target="#exampleModal"  class="btn btn-outline-dark"><i class="fa fa-plus-square-o fa-fw" aria-hidden="true"></i></button></td>
</tr>
</tbody>

</table>
    
  <table class="table table-sm table-borderless" id="myTable">

  <tbody>
    <?php 
while($res = mysqli_fetch_array($egroups)) {
echo "<tr>";
echo "<td><i class='fa fa-group fa-5x' aria-hidden='true'></i></td>";
echo "<td>". $res['groupname']. "</td>";
echo "<td>". $res['purpose']. "</td>";
echo "<td>". $res['descrip']. "</td>";
echo "<td>". $res['costcat']. "</td>";
echo "<td>". "keys: ". $res['positivekey'].",".$res['negativekey']. "</td>";
echo "</tr>";

}
?>
  </tbody>
</table>
    

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Employee Groups</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="" method="post">
        <div class="col-sm-10">
        <input type="text" name="gname" class="form-control"  placeholder="Group name">
      </div>
      <div class="col-sm-10">
        <input type="text" list="accounts" name="costcat" class="form-control" placeholder="Cost Category"  />
      <datalist id="accounts">
      <?php
      while($res = mysqli_fetch_array($result1)) {  
      echo "<option value='" . $res['accountno'] . "'>" . $res['accountname'] . "|" . $res['mainclass'] ."</option>";
      }
      ?>
      </datalist>
</div>
      <div class="col-sm-10">
        <input type="text" name="descrip" class="form-control" placeholder="Description">
      </div>
      <div class="col-sm-10">
        <input type="text" name="gpurpose" class="form-control" placeholder="Purpose">
      </div>
      <div class="col-sm-10">
        <input type="text" name="pkey" class="form-control" placeholder="Positive key">
      </div>

      <div class="col-sm-10">
        <input type="text" name="nkey" class="form-control" placeholder="Negative key">
      </div>
      

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="submit" class="btn btn-primary">Save group</button>
      </div>
    </form>
    </div>
  </div>
</div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://use.fontawesome.com/ae1f260992.js"></script>
<script>
$(document).ready(function(){
  $("#searchbox").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

  </body>
</html>
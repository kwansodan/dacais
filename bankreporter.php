<?php
require __DIR__ . '/readerauth.php';
?>

<?php 
include_once("config.php");
include_once("acfunctions.php");


if(isset($_POST['submit'])) {
	$querytype = mysqli_real_escape_string($mysqli, $_POST['querytype']);
	if($querytype=="char") {
		$schar = mysqli_real_escape_string($mysqli, $_POST['schar']);
		$stype = mysqli_real_escape_string($mysqli, $_POST['stype']);
		$fdate = mysqli_real_escape_string($mysqli, $_POST['fdate']);
		$tdate = mysqli_real_escape_string($mysqli, $_POST['tdate']);
		switch ($stype) {
	    case "em":
	        $result=mysqli_query($mysqli, "SELECT * FROM gl WHERE description = '".$schar."' AND (dateo BETWEEN '".$fdate."' AND '".$tdate."') ORDER BY dateo ASC");
			$resultsum=mysqli_fetch_array(mysqli_query($mysqli, "SELECT sum(amount) as gtotal FROM gl WHERE description = '".$schar."' AND (dateo BETWEEN '".$fdate."' AND '".$tdate."')"));
	        break;
	    case "sw":
	        $result=mysqli_query($mysqli, "SELECT * FROM gl WHERE description = '{$schar}%' AND (dateo BETWEEN '".$fdate."' AND '".$tdate."') ORDER BY dateo ASC");
			$resultsum=mysqli_fetch_array(mysqli_query($mysqli, "SELECT sum(amount) as gtotal FROM gl WHERE description LIKE '{$schar}%' AND (dateo BETWEEN '".$fdate."' AND '".$tdate."')"));
	        break;
	    case "cs":
	        $result=mysqli_query($mysqli, "SELECT * FROM gl WHERE description LIKE '%{$schar}%' AND (dateo BETWEEN '".$fdate."' AND '".$tdate."') ORDER BY dateo ASC");
			$resultsum=mysqli_fetch_array(mysqli_query($mysqli, "SELECT sum(amount) as gtotal FROM gl WHERE description LIKE '%{$schar}%' AND (dateo BETWEEN '".$fdate."' AND '".$tdate."')"));
	        break;
	     case "ew":
	       $result=mysqli_query($mysqli, "SELECT * FROM gl WHERE description LIKE '%{$schar}' AND (dateo BETWEEN '".$fdate."' AND '".$tdate."') ORDER BY dateo ASC");
			$resultsum=mysqli_fetch_array(mysqli_query($mysqli, "SELECT sum(amount) as gtotal FROM gl WHERE description LIKE '%{$schar}' AND (dateo BETWEEN '".$fdate."' AND '".$tdate."')"));
	        break;
	    				}
					}
	elseif($querytype=="spec_account"){
		$bank = mysqli_real_escape_string($mysqli, $_POST['bank']);
		$ttype = mysqli_real_escape_string($mysqli, $_POST['ttype']);
		$fdate = mysqli_real_escape_string($mysqli, $_POST['fdate']);
		$tdate = mysqli_real_escape_string($mysqli, $_POST['tdate']);


		$result=mysqli_query($mysqli, "SELECT * FROM gl WHERE acnumber = '".$bank."' AND (dateo BETWEEN '".$fdate."' AND '".$tdate."') ORDER BY dateo ASC");
		$resultsum=mysqli_fetch_array(mysqli_query($mysqli, "SELECT sum(amount) as gtotal FROM gl WHERE acnumber = '".$bank."' AND (dateo BETWEEN '".$fdate."' AND '".$tdate."')"));


		$actype = mysqli_query($mysqli, "SELECT * FROM coa WHERE accountno = '".$bank."' ");
		$actype = mysqli_fetch_array($actype);
}


}





?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>AIS</title>
    <?php 
   include 'header.php';
?>
  </head>
  <body>

    <div class="card border-light mb-3 w-75">
      <div class="card-header">Account Details</div>
  <div class="card-body">
    <p class="card-text">Account #: <?php echo $bank;?></p>
    <p class="card-text">Account Name: <?php echo $actype['accountname'];?></p>
    <p class="card-text">Period: <?php echo $fdate. " to ".$tdate;?></p>
    <p class="card-text">Opening balance: --</p>
      <p class="card-text">Closing balance: --</p>

      <table class="table table-sm" >
  <thead class="thead-dark">
    <tr>
      <th scope="col">Date</th>
      <th scope="col">Description</th>
      <th scope="col">Ref.</th>
      <th scope="col">Amount (GHS)</th>
      <th scope="col">Edit</th>
    </tr>
  </thead>
  <tbody id="myTable">
     <?php 
    //while($res = mysql_fetch_array($result)) { // mysql_fetch_array is deprecated, we need to use mysqli_fetch_array 
    while($res = mysqli_fetch_array($result)) {   

      // $reportamt = 'reportamt';
      // $amount = $reportamt($bank, $actype['accounttype'], $res['debitac'], $res['creditac'],$res['amount']);

        echo "<tr>";
        
        echo "<td>".$res['dateo']."</td>";
        echo "<td>".$res['description']."</td>";
        echo "<td>".$res['ref']."</td>";
        echo "<td>".number_format($res['amount'], 2, '.', ',')."</td>";
        echo "<td><a target='_blank' href=\"edit.php?id=$res[groupid]\">Edit</a></td>";
        echo "</tr>";
        
    }
    ?>
    <tr>
      <td colspan="2">Grand Total</td>

      <td colspan="1"><?php echo number_format($resultsum['gtotal'], 2, '.', ',')?></td>
    </tr>
  </tbody>
</table>  
  </div>



</div>
       
<br><br><br>
   

  
  </div>
</div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>
</html>

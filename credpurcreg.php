<?php

require __DIR__ . '/readerauth.php';
include_once("config.php");
$variablee = $_SESSION['aic'];
//fetching data in descending order (lastest entry first)
//$result = mysql_query("SELECT * FROM users ORDER BY id DESC"); // mysql_query is deprecated
$result = mysqli_query($mysqli, "SELECT * FROM coa WHERE accountno BETWEEN 2201 AND 2299"); // using mysqli_query instead 
$result1 = mysqli_query($mysqli, "SELECT * FROM coa WHERE accountno BETWEEN 5101 AND 5199"); 
$result2 = mysqli_query($mysqlii, "SELECT * FROM workers where activeness = 'active'");


if (!$result) {
    printf("Error: %s\n", mysqli_error($mysqli));
    exit();

}
?>
<?php
function generateRandomString($length = 10) {
    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
}


?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>JIMF AIS | PJ Entry</title>
      <?php 
   include 'header.php';
?>
  </head>
  <body>
    
    <div class="container">
    <h1> Credit Purchases </h1>

<form action="addpinvo.php" method="post">
  <div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="validationDefault01">Invoice Date/Purchase Date</label>
      <input type="date" class="form-control" id="dateofpurch" name="dateofpurch" required>
    </div>
  
  <div class="col-md-4 mb-3" id="supp" >
      <label for="validationDefault02">Purchase type</label>
      <select class="custom-select mr-sm-2" id="ptype" name="ptype" onchange="determiner(this.value)">
        <option selected>Choose...</option>
        <option value = "standard rated">Standard Rated</option>
        <option value="flat rated">Flat Rated</option>
        <option value = "no vat">No Vat</option>
      </select>
    </div>
    

<div class="col-md-4 mb-3">
      <label for="validationDefault02">Purchase Class</label>
      <input list="browsers" name="debitac" id="debitac" class="form-control" required>
  <datalist id="browsers">

<?php
while($res = mysqli_fetch_array($result1)) {  
echo "<option value='" . $res['accountno'] . "'>" . $res['accountname'] . "|" . $res['mainclass'] ."</option>";
}
?>

  </datalist>
    </div>
  </div>

<div class="form-row">
      
    
  </div>
<div class="form-row">
    <div class="col-md-4 mb-3" style="display: none;">
      <label for="validationDefault02">Purchase ID</label>
      <input type="text" class="form-control" id="purcid" name="purcid" value="<?php
          echo  generateRandomString();?>"  >
    </div>
  </div>
  
<div class="col-md-4 mb-3">
    <input type="text" name="groupid" id="groupid" placeholder="guid" class="form-control" />
    </div> 

<div class="form-row">
     <div class="col-md-4 mb-3">
      <label for="validationDefault02">Gross amount</label>
      <input type="text" class="form-control" id="gross" name="gross" required>
    </div>
    <div class="col-md-4 mb-3">
      <label for="validationDefault02">NHIL</label>
      <input type="text" class="form-control" id="nhil" name="nhil" value="0.00  " required>
    </div>
    <div class="col-md-4 mb-3">
      <label for="validationDefault02">GETFund</label>
      <input type="text" class="form-control" id="getf" name="getf" value="0.00  " required>
    </div>
  </div>

<div class="form-row">
<div class="col-md-4 mb-3">
      <label for="validationDefault02">Amount (Levies Incl.)</label>
      <input type="text" class="form-control" id="leviesincl" name="leviesincl" value="0.00  " required>
    </div>
    <div class="col-md-4 mb-3">
      <label for="validationDefault02">VAT</label>
      <input type="text" class="form-control" id="vat" name="vat" value="0.00  " required>
    </div>
    <div class="col-md-4 mb-3">
      <label for="validationDefault02">Amount (Taxes Incl.)</label>
      <input type="text" class="form-control" id="amount" name="amount" value="0.00  " required>
    </div>
  </div>

<div class="form-row">
   <div class="col-md-4 mb-3">
      <label for="validationDefault02">GRA Invoice #</label>
      <input type="text" class="form-control" id="grainvono" name="grainvono" value="" >
         </div>

    <div class="col-md-4 mb-3" id="supp" >
      <label for="validationDefault02">Select Supplier</label>
      <select class="custom-select mr-sm-2" id="creditac" name="creditac">
        <option selected>Choose...</option>
    <?php
  while($res = mysqli_fetch_array($result)) {  
echo "<option value='" . $res['accountno'] . "'>" . $res['accountname'] . "|" . $res['mainclass'] ."</option>";
      }
          ?>
      </select>
    </div>

    <div class="col-md-4 mb-3">
      <label for="exampleFormControlTextarea1">Notes</label>
    <textarea class="form-control" id="description" name="description" rows="1" required></textarea>
    </div>
  </div>


  <div class="form-row">
    <div class="col-md-12 mb-3" id="cust" >
      <label for="validationDefault02">Procurement officer</label>
      <input list="workers" name="assocperson" id="assocperson" class="form-control" required>
  <datalist id="workers">
        <?php
while($res = mysqli_fetch_array($result2)) {  
echo "<option value='" . $res['id'] . "'>" . $res['name']  ."</option>";
}
?>
  </datalist>
    </div>
    
       <div class="col-md-3 mb-3" style="display: none;">
      <label for="validationDefault02">Signature</label>
      <input type="text" class="form-control" id="signature" name="signature" <?php echo "value='".$variablee."'";?> >
    </div>
  </div>

  <button class="btn btn-primary" type="submit" name="submit">Enter</button>
</form>
</div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script>

<script>
  document.getElementById('dateofpurch').valueAsDate = new Date();
</script>

<script>
function determiner(ptype){
  if(ptype=="no vat"){
    // document.getElementById("nhil").readOnly = true;
    // document.getElementById("getf").readOnly = true;
    // document.getElementById("leviesincl").readOnly = true;
    // document.getElementById("vat").readOnly = true;
    // document.getElementById("amount").readOnly = true;
    // document.getElementById("grainvono").readOnly = true;
     $('input').keyup(function(){ // run anytime the value changes
    var firstValue  = Number($('#gross').val());   // get value of field
    
    document.getElementById('nhil').value = firstValue * 0.0;
    var nhil = document.getElementById('nhil').value;

    document.getElementById('getf').value = firstValue * 0.0;
    var getf = document.getElementById('getf').value;

    document.getElementById('leviesincl').value = parseFloat(firstValue) + parseFloat(nhil) + parseFloat(getf);
    document.getElementById('vat').value = parseFloat(document.getElementById('leviesincl').value) * 0.0;
    document.getElementById('amount').value = parseFloat(document.getElementById('vat').value) + parseFloat(document.getElementById('leviesincl').value);
// add them and output it
});
  }
    
  
  else if(ptype=="standard rated"){
    document.getElementById("grainvono").required = true;

    $('input').keyup(function(){ // run anytime the value changes
    var firstValue  = Number($('#gross').val());   // get value of field
    
    document.getElementById('nhil').value = firstValue * 0.025;
    var nhil = document.getElementById('nhil').value;

    document.getElementById('getf').value = firstValue * 0.025;
    var getf = document.getElementById('getf').value;

    document.getElementById('leviesincl').value = parseFloat(firstValue) + parseFloat(nhil) + parseFloat(getf);
    document.getElementById('vat').value = parseFloat(document.getElementById('leviesincl').value) * 0.125;
    document.getElementById('amount').value = parseFloat(document.getElementById('vat').value) + parseFloat(document.getElementById('leviesincl').value);
// add them and output it
});
  }

else if(ptype=="flat rated"){
    // document.getElementById("nhil").readOnly = true;
    // document.getElementById("getf").readOnly = true;
    // document.getElementById("leviesincl").readOnly = true;
    // document.getElementById("grainvono").required = true;
    $('input').keyup(function(){ // run anytime the value changes
   var firstValue  = Number($('#gross').val());   // get value of field
    
    document.getElementById('nhil').value = firstValue * 0.0;
    var nhil = document.getElementById('nhil').value;

    document.getElementById('getf').value = firstValue * 0.0;
    var getf = document.getElementById('getf').value;

    document.getElementById('leviesincl').value = parseFloat(firstValue) + parseFloat(nhil) + parseFloat(getf);
    document.getElementById('vat').value = parseFloat(document.getElementById('leviesincl').value) * 0.03;
    document.getElementById('amount').value = parseFloat(document.getElementById('vat').value) + parseFloat(document.getElementById('leviesincl').value);
// add them and output it
});
  }
}


</script>

  <script>
 document.getElementById("groupid").style.display = "none"; 
document.getElementById('groupid').value = Date.now() + Math.random();

</script>

  </body>
</html>

<?php
// require __DIR__ . '/readerauth.php';

include_once("config.php");
include_once("acfunctions.php");

$result = mysqli_query($mysqli, "SELECT DISTINCT accountno from coa ORDER BY accountno ASC");
?>

<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<title>General Ledger</title>
<?php 
include 'header.php';
?>
</head>
<body>

<div class="card card-body">
<h5 class="card-title">General Ledger</h5>

<p class="card-text"><?php echo date('Y-m-d H:i:s');?></p>
</div>

<table class="table table-sm header-fixed" >
<thead class="thead-dark">
<tr>
<th scope="col">A/C #</th>
<th scope="col">Date</th>
<th scope="col">ref.</th>
<th scope="col">Desc</th>
<th scope="col">Amount</th>

</tr>
</thead>
<tbody id="myTable">
<?php 
//while($res = mysql_fetch_array($result)) { // mysql_fetch_array is deprecated, we need to use mysqli_fetch_array 
while($res = mysqli_fetch_array($result)) {   

$acnumbers[]=$res['accountno']; 
} 

foreach($acnumbers as $acnumber){
//  		$actype = mysqli_query($mysqli, "SELECT * FROM coa WHERE accountno = '".$acnumber."' ");
// $actype = mysqli_fetch_array($actype);
$result2=mysqli_query($mysqli, "SELECT * FROM accountbalances WHERE account = '".$acnumber."' and  (baldate = DATE_FORMAT(NOW(),'%Y-01-01') or baldate = DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL 1 YEAR),'%Y-12-31'))");
$res2 = mysqli_fetch_array($result2);
$result1=mysqli_query($mysqli, "SELECT * FROM gl WHERE acnumber = '".$acnumber."'  AND dateo BETWEEN DATE_FORMAT(NOW(),'%Y-01-01') AND DATE_FORMAT(NOW(),'%Y-12-31') ORDER BY dateo ASC");

echo "<tr>";
echo "<td>".$acnumber."    ------     "."Opening bal.: ".$res2['bal']."</td>";

echo "</tr>";
while($res1 = mysqli_fetch_array($result1)) {  
// $reportamt = 'reportamt';
//  $amount = $reportamt($acnumber, $actype['accounttype'], $res1['debitac'], $res1['creditac'],$res1['amount']);
echo "<tr>";
echo "<td>".$acnumber."</td>";
echo "<td>".$res1['dateo']."</td>";
echo "<td>".$res1['ref']."</td>";
echo "<td>".$res1['description']."</td>";
echo "<td>".$res1['amount']."</td>";
echo "</tr>";
}  
echo "<tr>";
echo "<td></td>";
echo "<td></td>";
echo "<td>"."Closing bal.: "."</td>";
echo "<td>".$runningbal."</td>";
echo "</tr>";



echo "<tr>";
echo "<td></td>";
echo "<td></td>";
echo "<td></td>";
echo "<td></td>";
echo "</tr>";

}




?>
</tbody>
</table>
</div>
</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>

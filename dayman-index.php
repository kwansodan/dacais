<?php 

include_once("config.php");
include_once("acfunctions.php");
require __DIR__ . '/readerauth.php';


$result = mysqli_query($mysqli, "SELECT gl2.id, gl2.dateo, gl1.groupid, gl2.acnumber, coa.accountname, gl2.description, gl2.amount
FROM
(Select groupid FROM gl WHERE acnumber = 1001 and dateo=curdate()) gl1
LEFT JOIN 
(SELECT id, acnumber, description,dateo, groupid, amount FROM gl WHERE acnumber != 1001 and dateo=curdate()) gl2
ON gl1.groupid = gl2.groupid
LEFT JOIN 
(SELECT accountno, accountname FROM coa) coa
ON gl2.acnumber = coa.accountno
ORDER BY gl2.id ASC");


  $day_open = mysqli_fetch_array(mysqli_query($mysqli, "SELECT SUM(hr) as dayopen from(

  SELECT bal AS hr from accountbalances where account = 1001 AND (baldate = DATE_FORMAT(CURDATE(),'%Y-01-01') OR baldate = DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL 1 YEAR),'%Y-12-31'))
  union
  SELECT SUM(amount) AS hr from gl where acnumber = 1001 AND (dateo between DATE_FORMAT(CURDATE(),'%Y-01-01') AND (CURDATE()-1))
  )a"));

$td_nettr = mysqli_fetch_array(mysqli_query($mysqli, "SELECT SUM(amount) as nettr FROM gl where acnumber = 1001 AND dateo = CURDATE()"));

$dayclose = $day_open['dayopen'] + $td_nettr['nettr'];

?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Daily Log Sheet</title>
   <?php 
   include 'header.php';
?>
  </head>
  <body>
     
    
    
  <div class="card card-body">
  	<p></p>
       <button class="btn btn-outline-dark" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
    Expand/Shrink
  </button>
  </div>
 
</p>
<div class="collapse" id="collapseExample">
  <div class="card card-body">
  	<h5 class="card-title">Balance details</h5>
    
    <p class="card-text">Date: <?php echo date("Y/m/d");?></p>
    <p class="card-text">Opening bal: <?php echo $day_open['dayopen'];?></p>
    <p class="card-text">Net transactions value: <?php echo $td_nettr['nettr'];?></p>
    <p class="card-text">Closing balance: <?php echo $dayclose;?></p>
  </div>
</div>


   <table class="table table-sm" >
  <thead class="thead-dark">
    <tr>
      <th scope="col">Date</th>
      <th scope="col">Amount</th>
      <th scope="col">Class</th>
      <th scope="col">Description</th>
      <th scope="col">Balance</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody id="myTable">
     <?php 
    while($res = mysqli_fetch_array($result)) { 
    	$dayman_set = 'dayman';
    	$dayman_set = $dayman_set($res['amount']);
    	$runningbal += $dayman_set[0];
    	$show = $day_open['dayopen'] + $runningbal; 
        echo "<tr>";
        echo "<td>".$res['dateo']."</td>";
        echo "<td>".$dayman_set[1]."</td>";
        echo "<td>".$res['accountname']."</td>";
        echo "<td>".$res['description']."</td>";
        echo "<td>".number_format($show, 2, '.', ',')."</td>"; 
        echo "<td><a target='_blank' href=\"edit.php?id=$res[groupid]\">Edit</a></td>";
    }
    ?>
  </tbody>
</table>  

  
  </div>
</div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>
</html>
<?php 
include_once("config.php");
$customersres = mysqli_query($mysqli, "SELECT * FROM coa WHERE accountno BETWEEN 1100 AND 1200");




?>

<?php
if(isset($_POST['submit'])) {
$bank = mysqli_real_escape_string($mysqli, $_POST['bank']);
$fdate = mysqli_real_escape_string($mysqli, $_POST['fdate']);
$tdate = mysqli_real_escape_string($mysqli, $_POST['tdate']);


$result=mysqli_query($mysqli, "SELECT * FROM gl WHERE acnumber = '".$bank."' AND (dateo BETWEEN '".$fdate."' AND '".$tdate."')");

$result1 = mysqli_fetch_array(mysqli_query($mysqli, "SELECT opent.account, opent.yearopening, coalesce(abs(interimt.interim),0) AS spreadtotal, coalesce(abs(opent.yearopening),0) + coalesce(abs(interim),0) AS asatfromdate,periodt.netperiodtrans, coalesce(abs(opent.yearopening),0) + coalesce(abs(periud.netperiudtrans),0) AS closingbal from
(SELECT account, bal AS yearopening FROM accountbalances WHERE account = '".$bank."' AND (baldate = DATE_FORMAT(NOW(),'%Y-01-01') or baldate = DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL 1 YEAR),'%Y-12-31'))) opent
LEFT JOIN
(SELECT coalesce(abs(SUM(amount)),0) AS interim, acnumber FROM gl WHERE acnumber = '".$bank."' AND (dateo BETWEEN DATE_FORMAT(NOW(),'%Y-01-01') AND (DATE('".$fdate."')-1))) interimt
ON opent.account = interimt.acnumber
LEFT JOIN
(SELECT SUM(amount) AS netperiodtrans, acnumber FROM gl WHERE acnumber = '".$bank."' AND (dateo BETWEEN '".$fdate."' AND '".$tdate."')) periodt
ON opent.account = periodt.acnumber
LEFT JOIN
(SELECT SUM(amount) AS netperiudtrans, acnumber FROM gl WHERE acnumber = '".$bank."' AND (dateo BETWEEN DATE_FORMAT(NOW(),'%Y-01-01') AND '".$tdate."')) periud
ON opent.account = periud.acnumber"));

$result2=mysqli_fetch_array(mysqli_query($mysqli, "SELECT accountname FROM coa WHERE accountno = '".$bank."'"));
if (!$result1) {
    printf("Error: %s\n", mysqli_error($mysqli));
    exit();

}
}
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>DASHBOARD</title>
      <?php 
   include 'header.php';
?>
  </head>
  <body>
      <div class="container">

      <h1>Invoices   </h1>

<form action="" method="post">
  <div class="form-row">
    
<div class="col-md-3 mb-3" id="cust" >
      <label for="validationDefault02">Select Customer</label>
      <input list="browser" name="bank" id="bank" class="form-control" required>
  <datalist id="browser">
        <?php
while($res = mysqli_fetch_array($customersres)) {  
echo "<option value='" . $res['accountno'] . "'>" . $res['accountname'] . "|" . $res['mainclass'] ."</option>";
}
?>
  </datalist>
    </div>

     <div class="col-md-3 mb-3" >
      <label for="validationDefault01">From: </label>
      <input type="date" class="form-control" id="fdate" name="fdate"  required>
    </div>
    


    <div class="col-md-3 mb-3" >
      <label for="validationDefault01">To: </label>
      <input type="date" class="form-control" id="tdate" name="tdate"  required>
    </div>
</div>
  <button class="btn btn-primary" type="submit" name="submit">Query</button>
  
</form><br><br>


        <div class="col-md-4 mb-3">
      
      <input type="text" class="form-control" id="searchbox" placeholder="Search..." onkeyup="myFunction()">
      
    </div>

  <table class="table table-sm" >
    <div class="card card-body">

<h5 class="card-title">Statement of Account</h5>
<h6 class="card-title"><?php echo $result2['accountname']?></h6>
<p class="card-text">Bal as at <?php echo $fdate;?>: <?php echo "₵" . number_format($result1['asatfromdate'], 2, '.', ',')?></p>
<p class="card-text">Net transactions during period: <?php echo "₵" . number_format($result1['netperiodtrans'], 2, '.', ',')?></p>
<p class="card-text">Bal as at: <?php echo $tdate;?>: <?php echo "₵" . number_format($result1['closingbal'], 2, '.', ',')?></p>
</div>
  <thead class="thead-dark">
    <tr>
      <th scope="col">Date of Sale</th>
      <th scope="col">Customer</th>
      <th scope="col">Description</th>
      <th scope="col">Invoice Amount (₵)</th>
      <th scope="col">Balance (₵)</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody id="myTable">
     <?php 
    //while($res = mysql_fetch_array($result)) { // mysql_fetch_array is deprecated, we need to use mysqli_fetch_array 
    while($res = mysqli_fetch_array($result)) {  
    $runningbal += $res['amount'];
      $show = $result1['asatfromdate'] + $runningbal;       
        echo "<tr>";
        echo "<td>".$res['dateo']."</td>";
        echo "<td>".$res['acnumber']."</td>";
        echo "<td>".$res['description']."</td>";
        echo "<td>".number_format($res['amount'], 2, '.', ',')."</td>";
        echo "<td>".number_format($show, 2, '.', ',')."</td>";
        echo "<td><a href=\"bill.php?invoiceID=$res[groupid]\">Generate Invoice</a> </td>";  
              
    }
    ?>
    <tr ><td colspan="5">Printed: <?php echo date('Y-m-d H:i:s');?></td></tr>
  </tbody>
</table>
    

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("#searchbox").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

  </body>
</html>
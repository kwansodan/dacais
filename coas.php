<?php 

include_once("config.php");
include_once("acfunctions.php");
require __DIR__ . '/readerauth.php';


$result = mysqli_query($mysqli, "SELECT * from coa ORDER BY accountno");
?>

<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<title>Trial Balance</title>
<?php 
include 'header.php';
?>
</head>
<body>





<div class="card card-body">
<h5 class="card-title">Chart of Accounts</h5>

<p class="card-text"><?php echo date('Y-m-d H:i:s');?></p>
</div>



<table class="table table-sm" id="myTable" >

<div class="col-md-3 mb-3">
<input type="text" class="form-control" id="searchbox" placeholder="Search..." onkeyup="myFunction()">
</div>

<thead class="thead-dark">
<tr>
<th scope="col">Account #</th>
<th scope="col">Account name</th>
<th scope="col">Main Class</th>
<th scope="col">Subclass</th>
<th scope="col">Accounttype</th>
</tr>
</thead>
<tbody id="myTable">
<?php 
while($res = mysqli_fetch_array($result)) { 

echo "<tr>";
echo "<td>".$res['accountno']."</td>";
echo "<td>".$res['accountname']."</td>";
echo "<td>".$res['mainclass']."</td>";
echo "<td>".$res['subclass']."</td>";
echo "<td>".$res['accounttype']."</td>";
}
?>
</tbody>
</table>  


</div>
</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script>
$(document).ready(function(){
  $("#searchbox").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
</body>
</html>
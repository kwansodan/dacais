<?php 
include_once("config.php");
$suppliersres = mysqli_query($mysqli, "SELECT * FROM coa WHERE accountno BETWEEN 2200 AND 2299");

?>

<?php
if(isset($_POST['submit'])) {
$bank = mysqli_real_escape_string($mysqli, $_POST['bank']);
$fdate = mysqli_real_escape_string($mysqli, $_POST['fdate']);
$tdate = mysqli_real_escape_string($mysqli, $_POST['tdate']);


$result=mysqli_query($mysqli, "SELECT * FROM pjournal WHERE supplier = '".$bank."' AND (dateofpurchase BETWEEN '".$fdate."' AND '".$tdate."')");
if (!$result) {
    printf("Error: %s\n", mysqli_error($mysqli));
    exit();

}
}
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>DASHBOARD</title>
      <?php 
   include 'header.php';
?>
  </head>
  <body>
    
    <div class="container">
      <h1>Invoices   </h1>

<form action="" method="post">
  <div class="form-row">



    
<div class="col-md-3 mb-3" id="cust" >
      <label for="validationDefault02">Select Supplier</label>
      <input list="browser" name="bank" id="bank" class="form-control" required>
  <datalist id="browser">
        <?php
while($res = mysqli_fetch_array($suppliersres)) {  
echo "<option value='" . $res['accountno'] . "'>" . $res['accountname'] . "|" . $res['mainclass'] ."</option>";
}
?>
  </datalist>
    </div>


     <div class="col-md-3 mb-3" >
      <label for="validationDefault01">From: </label>
      <input type="date" class="form-control" id="fdate" name="fdate"  required>
    </div>
    


    <div class="col-md-3 mb-3" >
      <label for="validationDefault01">To: </label>
      <input type="date" class="form-control" id="tdate" name="tdate"  required>
    </div>


  </div>





  <button class="btn btn-primary" type="submit" name="submit">Query</button>
</form><br><br>


        <div class="col-md-4 mb-3">
      
      <input type="text" class="form-control" id="searchbox" placeholder="Search..." onkeyup="myFunction()">
      
    </div>

  <table class="table table-sm" >
  <thead class="thead-dark">
    <tr>
      <th scope="col">Date of Sale</th>
      <th scope="col">Supplier</th>
      <th scope="col">Description</th>
      <th scope="col">Invoice Amount (GHS)</th>
      <th scope="col">id</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody id="myTable">
     <?php 
    //while($res = mysql_fetch_array($result)) { // mysql_fetch_array is deprecated, we need to use mysqli_fetch_array 
    while($res = mysqli_fetch_array($result)) {         
        echo "<tr>";
        echo "<td>".$res['dateofpurchase']."</td>";
        echo "<td>".$res['supplier']."</td>";
        echo "<td>".$res['note']."</td>";
        echo "<td>".$res['taxincl']."</td>";
        echo "<td>".$res['id']."</td>";
        echo "<td><a href=\"editbill.php?invoice=$res[0]\">Edit</a></td>";  
              
    }
    ?>
  </tbody>
</table>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("#searchbox").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

  </body>
</html>
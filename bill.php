<?php

include_once("config.php");

$taxincl = mysqli_query($mysqli,"select * from gl
    where
    groupid = '".$_GET['invoiceID']."' and substring(acnumber, 1, 1) ='1'");
$invoice = mysqli_fetch_array($taxincl);

$query1 = mysqli_query($mysqli,"select * from coa where
    accountno = '".$invoice['acnumber']."'");
$recagent = mysqli_fetch_array($query1);

$taxexcl = mysqli_query($mysqli,"select abs(amount) as amount from gl
    where substring(acnumber, 1, 1) ='4' and
    groupid = '".$_GET['invoiceID']."'");
$glrecord_taxexcl = mysqli_fetch_array($taxexcl);

$nhil = mysqli_query($mysqli,"select abs(amount) as amount from gl
    where acnumber ='2002' and
    groupid = '".$_GET['invoiceID']."'");
$glrecord_nhil = mysqli_fetch_array($nhil);

$getf = mysqli_query($mysqli,"select abs(amount) as amount from gl
    where acnumber ='2001' and
    groupid = '".$_GET['invoiceID']."'");
$glrecord_getf = mysqli_fetch_array($getf);

$vat = mysqli_query($mysqli,"select abs(amount) as amount from gl
    where acnumber ='2003' and
    groupid = '".$_GET['invoiceID']."'");
$glrecord_vat = mysqli_fetch_array($vat);

if (!$query1) {
    printf("Error: %s\n", mysqli_error($mysqli));
    exit();

}
$customerd = mysqli_fetch_array($query1);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Jah is my Friend Ent.</title>
    <link rel="stylesheet" href="style.css" media="all" />
  </head>
  <body>
    <header class="clearfix">
      <div id="logo">
        <img src="logon.png">
      </div>
      <div id="company">
        <h2 class="name">Jah Is My Friend Enterprise</h2>
        <div>No 12 Brenya Street, Apenkwa</div>
        <div>030 293 7163</div>
        <div><a href="mailto:jehovahismyfriend18@gmail.com">jehovahismyfriend18@gmail.com</a></div>
      </div>
      </div>
    </header>
    <main>
      <div id="details" class="clearfix">
        <div id="client">
          <div class="to">INVOICE TO:</div>
          <?php
          echo "<h2 class='name'> " . $recagent['accountname'] . "</h2>";
          echo "<div class='email'> " . "-" . "</div>";
          ?>
          <!-- <h2 class="name">Paa Joe</h2> -->
          <!-- <div class="address">796 Silver Harbour, TX 79273, US</div> -->
          <!-- <div class="email"><a href="mailto:john@example.com">john@example.com</a></div> -->
        </div>

        <div id="invoice">
          <?php
          echo "<h1>INVOICE NO: # " .$invoice['ref']."</h1>"
          ?>
          <!-- <h1>INVOICE 3-2-1</h1> -->
          <?php
          echo "<div class='date'>Date of Invoice: " . $invoice['dateo'] . "</div>";
          ?>
          <?php
          echo "<div class='date'>Due Date: " . $invoice['dateo'] . "</div>";
          ?>
          <!-- <div class="date">Date of Invoice: $invoice['bill_date']</div> -->
          
        </div>
      </div>
      <table border="0" cellspacing="0" cellpadding="0">
        <thead>
          <tr>
            <th class="no">#</th>
            <th class="desc">DESCRIPTION</th>
            <!-- <th class="unit">UNIT PRICE</th>
            <th class="qty">QUANTITY</th> -->
            <th class="total">TOTAL</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="no">01</td>
            <td class="desc"><h3>Catering services</h3><?php echo $invoice['description'];?></td>
            <!-- <td class="unit">₵-</td>
            <td class="qty">-</td> -->
            <?php echo "<td class='total'>₵" . number_format($glrecord_taxexcl['amount'], 2, '.', ',') . "</td>";?>
            
          </tr>
         
        </tbody>
        <tfoot>
          <tr>
            <!-- <td colspan="2"></td> -->
            <td colspan="2">SUBTOTAL</td>
            <?php echo "<td>₵" . number_format($glrecord_taxexcl['amount'], 2, '.', ',') . "</td>";?>
            <!-- <td>$5,200.00</td> -->
          </tr>
          <!-- <tr>
            <td colspan="2"></td>
            <td colspan="2">Tourism levy 1%</td>
            <?php echo "<td>₵" . number_format($invoice['tlevy'], 2, '.', ',') . "</td>";?>
          </tr> -->
          <tr>
            <!-- <td colspan="2"></td> -->
            <td colspan="2">NHIL 2.5%</td>
            <?php echo "<td>₵" . number_format($glrecord_nhil['amount'], 2, '.', ',') . "</td>";?>
          </tr>
          <tr>
            <!-- <td colspan="2"></td> -->
            <td colspan="2">GETFL 2.5%</td>
            <?php echo "<td>₵" . number_format($glrecord_getf['amount'], 2, '.', ',') . "</td>";?>
           <!--  <td>$1,300.00</td> -->
          </tr>
          <tr>
            <!-- <td colspan="2"></td> -->
            <td colspan="2">NHIL + GETFL 5%</td>
            <?php echo "<td>₵" . number_format($glrecord_nhil['amount']+$glrecord_getf['amount'], 2, '.', ',') . "</td>";?>
            <!-- <td>$1,300.00</td> -->
          </tr>
          <tr>
            <!-- <td colspan="2"></td> -->
            <td colspan="2">VAT 12.5%</td>
            <?php echo "<td>₵" . number_format($glrecord_vat['amount'], 2, '.', ',') . "</td>";?>
            <!-- <td>$1,300.00</td> -->
          </tr>
          <tr>
            <!-- <td colspan="2"></td> -->
            <td colspan="2">GRAND TOTAL</td>
            <?php echo "<td>₵" . number_format($invoice['amount'], 2, '.', ',') . "</td>";?>
            <!-- <td>$6,500.00</td> -->
          </tr>
        </tfoot>
      </table>
      <div id="thanks">Thank you!</div>
      <div id="notices">
        <div>NOTICE:</div>
        <div class="notice">Make all checks payable to Jah is my Friend Ent.</div>
        <div class="notice">If you have any questions concerning this invoice, use the following contact information: 030 293 3658</div>
      </div>
    </main>
    <footer>
      Invoice was created on a computer and is valid without the signature and seal.
    </footer>
  </body>
</html>
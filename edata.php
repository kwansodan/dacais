<?php 
include_once("config.php");
include_once("acfunctions.php");
$edata = mysqli_query($mysqli, "SELECT * FROM ebiodata where doe IS NULL OR doe > CURDATE()");
$egroups = mysqli_query($mysqli, "SELECT * FROM egroups");
?>

<?php
if(isset($_POST['submit'])) { 

  $fname = mysqli_real_escape_string($mysqli, $_POST['fname']);
  $sname = mysqli_real_escape_string($mysqli, $_POST['sname']);
  $mname = mysqli_real_escape_string($mysqli, $_POST['mname']);
  $gender = mysqli_real_escape_string($mysqli, $_POST['gender']);
  $maritalstatus = mysqli_real_escape_string($mysqli, $_POST['maritalstatus']);
  $tel = mysqli_real_escape_string($mysqli, $_POST['tel']);
  $resaddress = mysqli_real_escape_string($mysqli, $_POST['resaddress']);
  $egroup = mysqli_real_escape_string($mysqli, $_POST['egroup']);
    $ssnitid = mysqli_real_escape_string($mysqli, $_POST['ssnitid']);
  $tin = mysqli_real_escape_string($mysqli, $_POST['tin']);
  $bank = mysqli_real_escape_string($mysqli, $_POST['bank']);
  $accountname = mysqli_real_escape_string($mysqli, $_POST['accountname']);
    $accountno = mysqli_real_escape_string($mysqli, $_POST['accountno']);
  $payday = mysqli_real_escape_string($mysqli, $_POST['payday']);
  $desig = mysqli_real_escape_string($mysqli, $_POST['desig']);
  $dob = mysqli_real_escape_string($mysqli, $_POST['dob']);
   $dor = mysqli_real_escape_string($mysqli, $_POST['dor']);


   $result = mysqli_query($mysqli, "INSERT INTO ebiodata(fname, sname, mname, gender, maritalstatus, tel, resaddress, egroup, ssnitid, tin, bankname, accountname, accountno, payday, designation, dob, dor) VALUES('$fname','$sname','$mname','$gender','$maritalstatus','$tel','$resaddress','$egroup','$ssnitid','$tin','$bank','$accountname','$accountno','$payday','$desig','$dob','$dor')");

   $l_empee = mysqli_fetch_array(mysqli_query($mysqli, "SELECT * FROM ebiodata ORDER BY id DESC LIMIT 1"));

   $result = mysqli_query($mysqli, "INSERT INTO salarydets(empeeid) VALUES('".$l_empee['id']."')");

    if (!$result) {
    printf("Error: %s\n", mysqli_error($mysqli));
    exit();

}else{
    echo "<script type='text/javascript'> document.location = 'edata.php'; </script>";
    exit();
  }

}



?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Employees</title>
      <?php 
   include 'header.php';
?>
  </head>
  <body>
      <div class="container">

<table class="table table-sm "> 
  <!-- <thead>
    <tr>
      <th scope="col"></th>
      <th scope="col"></th>
      <th scope="col"></th>
      <th scope="col"></th>
      <th scope="col"></th>
      <th scope="col"></th>
      <th scope="col"></th>
    </tr>
  </thead> -->
  <tbody>
    <tr>
 
  <td ><div class="col-md-4 mb-3"><input type="text" class="form-control" id="searchbox" placeholder="Search..."  onkeyup="myFunction()"></div></td>
   <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
       <td></td>
      <td></td>
      <td></td>
       <td></td>
      <td></td>
      <td></td>
  <td><button type="button" data-toggle="modal" data-target="#createModalLong"  class="btn btn-outline-dark"><i class="fa fa-plus-square-o fa-fw" aria-hidden="true"></i></button></td>
</tr>
  </tbody>

</table>
    

  <table class="table table-sm table-hover" id="myTable">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Surname</th>
      <th scope="col">Division</th>
      <th scope="col">Tel.</th>
      <th scope="col"></th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
      
       <?php 
while($res = mysqli_fetch_array($edata)) {
echo "<tr>";
echo "<th scope='row'>1</th>";
echo "<td>". $res['fname']. "</td>";
echo "<td>". $res['tel']. "</td>";
echo "<td>". $res['resstatus']. "</td>";
echo "<td>". $res['egroup']. "</td>";
echo "<td>". $res['payday']. "</td>";
echo "<td><button type='button' data-toggle='modal' data-target='#viewModalLong' name='add' id='add'  class='btn btn-outline-dark'><i class='fa fa-eye fa-fw' aria-hidden='true'></i></button></td>";
echo "<td><button type='button' data-toggle='modal' data-target='#updateModalLong'  name='add' id='add'  class='btn btn-outline-dark'><i class='fa fa-pencil-square-o fa-fw' aria-hidden='true'></i></button></td>";
echo "</tr>";
}
?>  
  </tbody>
</table>
    

<!-- Create Modal -->
<div class="modal fade" id="createModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Create employee</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                <form action="" method="post">
<div class="form-row">
    <div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="First Name" id="fname" name="fname" autocomplete="off" required>
    </div>

    <div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="Surname" id="sname"  name="sname" autocomplete="off" required>
    </div>
</div>

<div class="form-row">
     <div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="Middle Name" id="mname"  name="mname" autocomplete="off" required>
    </div>

    <div class="col-md-6 mb-3">
      <select class="custom-select mr-sm-2" id="gender" name="gender"  >
        <option value="">Sex</option>
        <option value="male">Male</option>
        <option value="female">Female</option>        
      </select>
    </div>
</div>

<div class="form-row">
    <div class="col-md-6 mb-3">
      <select class="custom-select mr-sm-2" id="maritalstatus" name="maritalstatus"  >
        <option value="">Marital status</option>
        <option value="married">Married</option>
        <option value="single">Single</option>        
      </select>
    </div>

     <div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="Tel" id="tel"  name="tel" autocomplete="off" required>
    </div>
</div>

<div class="form-row">
<div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="Residential address" id="resaddress"  name="resaddress" autocomplete="off" required>
    </div>
<div class="col-md-6 mb-3">
 <input type="text" list="egroups" name="egroup" class="form-control" placeholder="Employee group" >
      <datalist id="egroups">
      <?php
      while($egroups = mysqli_fetch_array($egroups)) {  
      echo "<option value='" . $egroups['id'] . "'>" . $egroups['groupname'] . "|" . $egroups['purpose'] ."</option>";
      }
      ?>
      </datalist>
    </div>
</div>

<div class="form-row">
<div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="SSNIT ID" id="ssnitid"  name="ssnitid" autocomplete="off" required>
    </div>

    <div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="TIN" id="tin"  name="tin" required>
    </div>
</div>

<div class="form-row">
<div class="col-md-6 mb-3">
<input type="text" list="banks" name="bank" class="form-control" placeholder="Employee bank" >
      <datalist id="banks">
      <option value="ABGL">Absa Bank Ghana Limited</option>
    <option value="ABP">Access Bank (Ghana) Plc</option>
    <option value="ADBL">Agricultural Development Bank Limited</option>
    <option value="ATM">AirtelTigo Money</option>
    <option value="BAGL">Bank of Africa Ghana Limited</option>
    <option value="CBL">CAL Bank Limited</option>
    <option value="CBGL">Consolidated Bank Ghana Limited</option>
    <option value="EGL">Ecobank Ghana Limited</option>
    <option value="FL">FBNBank (Ghana) Limited</option>
    <option value="FBGL">Fidelity Bank Ghana Limited</option>
    <option value="FABL">First Atlantic Bank Limited</option>
    <option value="FNBL">First National Bank (Ghana) Limited</option>
    <option value="GBL">GCB Bank Limited</option>
    <option value="GHLBL">GHL Bank Limited</option>
    <option value="GTBL">Guaranty Trust Bank (Ghana) Limited</option>
    <option value="MOMO">MTN Mobile Money</option>
    <option value="NIBL">National Investment Bank Limited</option>
    <option value="OBGL">OmniBSIC Bank Ghana Limited</option>
    <option value="PBL">Prudential Bank Limited</option>
    <option value="RBL">Republic Bank (Ghana) Limited</option>
    <option value="SGL">Societe General (Ghana) Limited</option>
    <option value="SBGL">Stanbic Bank Ghana Limited</option>
    <option value="SCBL">Standard Chartered Bank (Ghana) Limited</option>
    <option value="UBAL">United Bank for Africa (Ghana) Limited</option>
    <option value="UMBL">Universal Merchant Bank Limited</option>
    <option value="VC">Vodafone Cash</option>
    <option value="ZBL">Zenith Bank (Ghana) Limited</option>
      </datalist>
    </div>

    <div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="Bank account name" id="accountname"  name="accountname" autocomplete="off" required>
    </div>
</div>

<div class="form-row">
<div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="Bank account #" id="accountno"  name="accountno" required>
    </div>

    <div class="col-md-6 mb-3">
      <!-- <input type="text" class="form-control" placeholder="Pay day" id="dateofinvo"  name="dateofinvo" required> -->
    </div>
</div>

<div class="form-row">
<div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="Pay day" id="payday"  name="payday" required>
    </div>

    <div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="Designation" id="desig"  name="desig" required>
    </div>
</div>
<div class="form-row">
<div class="col-md-6 mb-3">
  <label for="validationDefault01">Date of birth</label>
      <input type="date" class="form-control"  id="dob"  name="dob" required>
    </div>

    <div class="col-md-6 mb-3">
      <label for="validationDefault01">Date of join</label>
      <input type="date" class="form-control"  id="dor"  name="dor" required>
    </div>
</div>
<div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Abort</button>
        <button type="submit" name="submit" class="btn btn-primary">Create</button>
      </div>
</form>
      </div>
      
    </div>
  </div>
</div>



<!--View Modal -->
<div class="modal fade" id="viewModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">View employee</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                <form action="addsinvo.php" method="post">
<div class="form-row">
    <div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="First Name" id="dateofsale" name="dateofsale" required>
    </div>

    <div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="Surname" id="dateofinvo"  name="dateofinvo" required>
    </div>
</div>

<div class="form-row">
     <div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="Middle Name" id="dateofinvo"  name="dateofinvo" required>
    </div>

    <div class="col-md-6 mb-3">
      <select class="custom-select mr-sm-2" id="stype" name="stype" onchange="determiner(this.value)" >
        <option value="">Sex</option>
        <option value="male">Male</option>
        <option value="female">Female</option>        
      </select>
    </div>
</div>

<div class="form-row">
    <div class="col-md-6 mb-3">
      <select class="custom-select mr-sm-2" id="stype" name="stype" onchange="determiner(this.value)" >
        <option value="">Marital status</option>
        <option value="male">Married</option>
        <option value="female">Single</option>        
      </select>
    </div>

     <div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="Surname" id="dateofinvo"  name="dateofinvo" required>
    </div>
</div>

<div class="form-row">
<div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="Residential address" id="dateofinvo"  name="dateofinvo" required>
    </div>
<div class="col-md-6 mb-3">
      <select class="custom-select mr-sm-2" id="stype" name="stype" >
        <option value="">Employee group</option>
        <option value="male">Kitchen</option>
        <option value="female">Chop bar</option>        
      </select>
    </div>
</div>

<div class="form-row">
<div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="SSNIT ID" id="dateofinvo"  name="dateofinvo" required>
    </div>

    <div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="TIN" id="dateofinvo"  name="dateofinvo" required>
    </div>
</div>

<div class="form-row">
<div class="col-md-6 mb-3">
      <select class="custom-select mr-sm-2" id="stype" name="stype" >
        <option value="">Employee bank</option>
        <option value="male">Absa</option>
        <option value="female">NIB</option>        
      </select>
    </div>

    <div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="Bank account name" id="dateofinvo"  name="dateofinvo" required>
    </div>
</div>

<div class="form-row">
<div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="Bank account #" id="dateofinvo"  name="dateofinvo" required>
    </div>

    <div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="Bank account name" id="dateofinvo"  name="dateofinvo" required>
    </div>
</div>

<div class="form-row">
<div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="Pay day" id="dateofinvo"  name="dateofinvo" required>
    </div>

    <div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="Designation" id="dateofinvo"  name="dateofinvo" required>
    </div>
</div>
<div class="form-row">
<div class="col-md-6 mb-3">
  <label for="validationDefault01">Date of birth</label>
      <input type="date" class="form-control" placeholder="Date of" id="dateofinvo"  name="dateofinvo" required>
    </div>

    <div class="col-md-6 mb-3">
      <label for="validationDefault01">Date of join</label>
      <input type="date" class="form-control" placeholder="Date of" id="dateofinvo"  name="dateofinvo" required>
    </div>
</div>
<div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Exit</button>
      </div>
</form>
      </div>
      
    </div>
  </div>
</div>


<!--Update Modal -->
<div class="modal fade" id="updateModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Update employee record</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                <form action="addsinvo.php" method="post">
<div class="form-row">
    <div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="First Name" id="dateofsale" name="dateofsale" required>
    </div>

    <div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="Surname" id="dateofinvo"  name="dateofinvo" required>
    </div>
</div>

<div class="form-row">
     <div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="Middle Name" id="dateofinvo"  name="dateofinvo" required>
    </div>

    <div class="col-md-6 mb-3">
      <select class="custom-select mr-sm-2" id="stype" name="stype" onchange="determiner(this.value)" >
        <option value="">Sex</option>
        <option value="male">Male</option>
        <option value="female">Female</option>        
      </select>
    </div>
</div>

<div class="form-row">
    <div class="col-md-6 mb-3">
      <select class="custom-select mr-sm-2" id="stype" name="stype" onchange="determiner(this.value)" >
        <option value="">Marital status</option>
        <option value="male">Married</option>
        <option value="female">Single</option>        
      </select>
    </div>

     <div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="Surname" id="dateofinvo"  name="dateofinvo" required>
    </div>
</div>

<div class="form-row">
<div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="Residential address" id="dateofinvo"  name="dateofinvo" required>
    </div>
<div class="col-md-6 mb-3">
      <select class="custom-select mr-sm-2" id="stype" name="stype" >
        <option value="">Employee group</option>
        <option value="male">Kitchen</option>
        <option value="female">Chop bar</option>        
      </select>
    </div>
</div>

<div class="form-row">
<div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="SSNIT ID" id="dateofinvo"  name="dateofinvo" required>
    </div>

    <div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="TIN" id="dateofinvo"  name="dateofinvo" required>
    </div>
</div>

<div class="form-row">
<div class="col-md-6 mb-3">
      <select class="custom-select mr-sm-2" id="stype" name="stype" >
        <option value="">Employee bank</option>
        <option value="male">Absa</option>
        <option value="female">NIB</option>        
      </select>
    </div>

    <div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="Bank account name" id="dateofinvo"  name="dateofinvo" required>
    </div>
</div>

<div class="form-row">
<div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="Bank account #" id="dateofinvo"  name="dateofinvo" required>
    </div>

    <div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="Bank account name" id="dateofinvo"  name="dateofinvo" required>
    </div>
</div>

<div class="form-row">
<div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="Pay day" id="dateofinvo"  name="dateofinvo" required>
    </div>

    <div class="col-md-6 mb-3">
      <input type="text" class="form-control" placeholder="Designation" id="dateofinvo"  name="dateofinvo" required>
    </div>
</div>
<div class="form-row">
<div class="col-md-6 mb-3">
  <label for="validationDefault01">Date of birth</label>
      <input type="date" class="form-control" placeholder="Date of" id="dateofinvo"  name="dateofinvo" required>
    </div>

    <div class="col-md-6 mb-3">
      <label for="validationDefault01">Date of join</label>
      <input type="date" class="form-control" placeholder="Date of" id="dateofinvo"  name="dateofinvo" required>
    </div>
</div>
<div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Abort</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
</form>
      </div>
      
    </div>
  </div>
</div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://use.fontawesome.com/ae1f260992.js"></script>
<script>
$(document).ready(function(){
  $("#searchbox").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

  </body>
</html>
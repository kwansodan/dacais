<nav class="navbar sticky-top navbar-expand-lg navbar navbar-dark bg-dark" >
  <a class="navbar-brand" href="/index.php"><img src="jimflogo.png" width="70" height="50" class="d-inline-block align-top" alt="" loading="lazy"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link active" href="/entry.php">Journal <span class="sr-only">(current)</span></a>

       <!-- <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Cash book
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="/dayman-index.php">Cash transactions</a>
          <a class="dropdown-item" href="/bankq.php">Bank transactions</a>
        </div>
      </li> -->

      <a class="nav-item nav-link " href="/credsalesreg.php">Credit sale <span class="sr-only">(current)</span></a>
      <a class="nav-item nav-link " href="/credpurcreg.php">Credit Purchase<span class="sr-only">(current)</span></a>
       <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Receivables
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="/credsales.php">Receivables</a>
<!--           <a class="dropdown-item" href="/customerreg.php">New RA</a> -->
          <a class="dropdown-item" href="/custregistry.php">Receivable agents</a>
          <div class="dropdown-divider"></div>
          <!-- <a class="dropdown-item" href="#">Something else here</a> -->
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Payables
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="/credpurc.php">Payables</a>
          <a class="dropdown-item" href="/suppliers.php">Payable agents</a>
          <div class="dropdown-divider"></div>
          
        </div>
      </li>


      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Compensation
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="/egroups.php">New employee</a>

          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="/edata.php">Employee data</a>
          <!-- <a class="dropdown-submenu dropdown-item dropdown-toggle " data-toggle="dropdown" href="/suppliers.php">Employee</a>
          <ul class="dropdown-menu">
              <a class="dropdown-item" href="#">Create</a>
              <a class="dropdown-item" href="#">Update</a>
              <a class="dropdown-item" href="#">Activate / Inactivate</a>
            </ul> -->
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="/pheads.php">Payroll run</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="/saldets.php">Approvals</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="/rpaye.php">PAYE Report</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="rssnit.php">SSNIT Report</a>
          
      </li>


      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Dashboards
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="/dayman-index.php">Dayman Index - Cash </a>
          <a class="dropdown-item" href="/dib.php">Dayman Index - Bank</a>
          <!-- <a class="dropdown-item" href="#">Others</a> -->
        </div>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Query
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="/bankq.php">Transactional </a>
          <a class="dropdown-item" href="/tbal.php">Trial balance</a>
          <a class="dropdown-item" href="/full.php">General Ledger</a>
        </div>
      </li>

      

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Settings
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          
          <a class="dropdown-item" href="/coas.php">Chart of Accounts</a>
          <a class="dropdown-item" href="/newac.php">New Account</a>
          <a class="dropdown-item" href="/#.php">PAYE Config</a>
        </div>
      </li>

      

      <!-- <a class="nav-item nav-link active" href="/dayman-index.php">Mainpage <span class="sr-only">(current)</span></a> -->
      



    </div>
   
  </div>
</nav>



<?php
$mdw = 319;
$tieronerate = 0.135;
function reportedBasic($basic, $allowance){
	$total = $basic + $allowance;
	if($total > $GLOBALS['mdw']){
		$base = $basic;
	}
	elseif($total<$GLOBALS['mdw']){
		$base = $GLOBALS['mdw'];
	}
	elseif($total==$GLOBALS['mdw']){
		$base = $basic;
	}
	return round($base);
}

function tierOne($base){
	$tierone = $base * $GLOBALS['tieronerate'];
	return round($tierone,2);
}

function takeHome($basic,$allowa,$emppeessnit, $tax, $rent){
	$takehome = $basic + $allowa - $emppeessnit - $tax -$rent;
	return round($takehome,2);
}


function rentEffect($resstatus){
	if($resstatus == "Resident"){
		return round(10.00,2);
	}
	elseif($resstatus=="Non-resident"){
		return 0.00;
	}
}

function ssnitempeeComputer($status,$base){
	$ssfemprate = 0.055;
$ssfemperrate = 0.13 ;
	if ($status == "yes"){
		$ssfempee = $base * $ssfemprate;
		// $ssfemper = $base * $ssfemperrate;
		return round($ssfempee,2);
	}
	elseif($status == "no"){
		return 0.00;
	}
	elseif(is_numeric($status)){
		return round($status,2);
	}
}
function actualComputer($base, $allowance, $activeness, $spv){

$totalemol = $base + $allowance;
$ssfempee = 'ssnitempeeComputer';
$chargeable = $totalemol - $ssfempee($spv, $activeness, $base);
// $allowance = $worker['allowance'];
// $spv = $worker['spvstatus'];
$bandsize = array(
"band1"=> 319,
"band2"=> 100,
"band3"=> 120,
"band4"=> 3000,
"band5"=> 16461,
"band6"=> 20000,
	);

$bandsizecum = array(
"band1"=> $bandsize["band1"],
"band2"=> $bandsize["band2"] +$bandsize["band1"],
"band3"=> $bandsize["band3"] +$bandsize["band2"]+$bandsize["band1"],
"band4"=> $bandsize["band4"] +$bandsize["band3"]+$bandsize["band2"]+$bandsize["band1"],
"band5"=> $bandsize["band5"] +$bandsize["band4"]+$bandsize["band3"]+$bandsize["band2"]+$bandsize["band1"],

);

$bandrates = array(
"band1"=> 0.00,
"band2"=> 0.05,
"band3"=> 0.10,
"band4"=> 0.175,
"band5"=> 0.25,
"band6"=> 0.30,
);
$tax = array(
"band1"=> $bandrates["band1"] * $bandsize["band1"] ,
"band2"=> $bandrates["band2"] * $bandsize["band2"],
"band3"=> $bandrates["band3"] * $bandsize["band3"],
"band4"=> $bandrates["band4"] * $bandsize["band4"],
"band5"=> $bandrates["band5"] * $bandsize["band5"],

);

$cumtax = array(
"band1"=> $tax["band1"],
"band2"=> $tax["band2"] + $tax["band1"],
"band3"=> $tax["band3"] + $tax["band2"] + $tax["band1"],
"band4"=> $tax["band4"] + $tax["band3"] + $tax["band2"] + $tax["band1"],
"band5"=> $tax["band5"] + $tax["band4"] + $tax["band3"] + $tax["band2"] + $tax["band1"],

);
if ($chargeable>$bandsizecum["band5"]) {
$paye =($chargeable-$bandsizecum["band5"]) * ($bandrates["band6"]) + $cumtax["band5"];
}

elseif ($chargeable>$bandsizecum["band4"]) {
$paye =($chargeable-$bandsizecum["band4"]) * ($bandrates["band5"]) + $cumtax["band4"];
}

elseif ($chargeable>$bandsizecum["band3"]) {
$paye =($chargeable-$bandsizecum["band3"]) * ($bandrates["band4"]) + $cumtax["band3"];
}

elseif ($chargeable>$bandsizecum["band2"]) {
$paye =($chargeable-$bandsizecum["band2"]) * ($bandrates["band3"]) + $cumtax["band2"];
}

elseif ($chargeable>$bandsizecum["band1"]) {
$paye = ($chargeable-$bandsizecum["band1"]) * ($bandrates["band2"]) ;
}

else {
$paye = 0;
}
return round($paye,2);

}
?>
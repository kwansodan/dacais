<?php
//including the database connection file
include_once("config.php");
require __DIR__ . '/readerauth.php';
$variablee = $_SESSION['aic'];

$result1 = mysqli_query($mysqli, "SELECT * FROM coa WHERE accountno BETWEEN 1100 AND 1200");
$result = mysqli_query($mysqli, "SELECT * FROM coa WHERE accountno BETWEEN 4000 AND 4999"); // using mysqli_query instead  
$result2 = mysqli_query($mysqlii, "SELECT * FROM workers where activeness = 'active'");


if (!$result1) {
    printf("Errror: %s\n", mysqli_error($mysqli));
    exit();
}
?>

<?php
function generateRandomString($length = 10) {
    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
}


?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Transactions</title>
<?php 
   include 'header.php';
?>
  </head>
  <body>
    
    <div class="container">
    <h1> Credit Sales </h1>
<form action="addsinvo.php" method="post">
  <div class="form-row">

  	<div class="col-md-3 mb-3">
      <label for="validationDefault01">Date of Sale</label>
      <input type="date" class="form-control" id="dateofsale" name="dateofsale" required>
    </div>

    <div class="col-md-3 mb-3">
      <label for="validationDefault01">Date of Invoice</label>
      <input type="date" class="form-control" id="dateofinvo"  name="dateofinvo" required>
    </div>

    
  <div class="col-md-3 mb-3">
      <label for="validationDefault02">Sales type</label>
      <select class="custom-select mr-sm-2" id="stype" name="stype" onchange="determiner(this.value)" >
        <option value="">Select an option</option>
        <option value="RESSALE">Restaurant sale</option>
        <option value="CHBSALE">Chop bar sale</option>        
      </select>
    </div>  
    <div class="col-md-3 mb-3">
      <label for="validationDefault02">Sales Class</label>
      <input list="browsers" name="creditac" id="creditac" class="form-control" required>
  <datalist id="browsers">

<?php
while($res = mysqli_fetch_array($result)) {  
echo "<option value='" . $res['accountno'] . "'>" . $res['accountname'] . "|" . $res['mainclass'] ."</option>";
}
?>

  </datalist>
    </div>
  </div>


  
<div class="form-row">

   <div class="col-md-3 mb-3"> <label for="validationDefault02">Gross Amount</label> 
    <input type="text" class="form-control" id="gross" name="gross" value="0.00" autocomplete="off" required> 
  </div>

  <div class="col-md-3 mb-3"> <label for="validationDefault02">NHIL</label> 
    <input type="text" class="form-control" id="nhil" name="nhil" value="0.00" required> 
  </div>

  <div class="col-md-3 mb-3"> <label for="validationDefault02">GETFund</label> 
    <input type="text" class="form-control" id="getf" name="getf" value="0.00" required> 
  </div>

   <div class="col-md-3 mb-3"> <label for="validationDefault02">Levies Incl.</label> 
    <input type="text" class="form-control" id="leviesincl" name="leviesincl" value="0.00" required> 
  </div> 
</div>

<div class="form-row">
  <div class="col-md-3 mb-3"> <label for="validationDefault02">VAT</label> 
    <input type="text" class="form-control" id="vat" name="vat" value="0.00" required> 
  </div>

    <div class="col-md-3 mb-3"> <label for="validationDefault02">Taxes Incl.</label> 
    <input type="text" class="form-control" id="amount" name="amount" value="0.00" required> 
  </div>

  <div class="col-md-3 mb-3">
      <label for="validationDefault02">GRA Invoice #</label>
      <input list="encodings" type="text" class="form-control" id="grainvono" name="grainvono" value="" autocomplete="off" >
      <datalist id="encodings">
        <option value = "tnum" selected>NA</option>
      </datalist>
    </div>

    <div class="col-md-3 mb-3">
      <label for="exampleFormControlTextarea1">Note</label>
    <textarea class="form-control" id="description" name="description" rows="1" required></textarea>
    </div>
  </div>


 <div class="col-md-4 mb-3">
    <input type="text" name="groupid" id="groupid" placeholder="guid" class="form-control" />
    </div> 
  
<div class="form-row">


    <div class="col-md-6 mb-3" id="cust" >
      <label for="validationDefault02">Select Customer</label>
      <input list="browser" name="debitac" id="debitac" class="form-control" autocomplete="off" required>
  <datalist id="browser">
        <?php
while($res = mysqli_fetch_array($result1)) {  
echo "<option value='" . $res['accountno'] . "'>" . $res['accountname'] . "|" . $res['mainclass'] ."</option>";
}
?>
  </datalist>
    </div>
    
    <div class="col-md-6 mb-3" id="cust" >
      <label for="validationDefault02">Sales person</label>
      <input list="workers" name="assocperson" id="assocperson" class="form-control" autocomplete="off" required>
  <datalist id="workers">
        <?php
while($res = mysqli_fetch_array($result2)) {  
echo "<option value='" . $res['id'] . "'>" . $res['name']  ."</option>";
}
?>
  </datalist>
    </div>


     <div class="col-md-3 mb-3" style="display: none;">
      <label for="validationDefault02">Signature</label>
      <input type="text" class="form-control" id="signature" name="signature" <?php echo "value='".$variablee."'";?> required>
    </div>


     <div class="col-md-4 mb-3" style="display: none;">
     <label for="validationDefault02">Credit sale identifier</label>
     <input type="text" class="form-control" id="saleid" name="saleid" value="<?php
      echo  generateRandomString();?>">
  </div>

  </div>




  <button class="btn btn-primary" type="submit" name="submit">Enter</button>
  </div>
</form>
</div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<script>
  document.getElementById('dateofsale').valueAsDate = new Date();
</script>
<script>
  document.getElementById('dateofinvo').valueAsDate = new Date();
</script>
<script>
function determiner(stype){
  if(stype=="RESSALE"){
    document.getElementById("nhil").readOnly = false;
    document.getElementById("getf").readOnly = false;
    document.getElementById("leviesincl").readOnly = false;
    document.getElementById('vat').readOnly = false;
    document.getElementById('amount').readOnly = false;
    document.getElementById("grainvono").readOnly = false;
    document.getElementById("grainvono").required = false;

    $('input').keyup(function(){ // run anytime the value changes
    var gross  = Number($('#gross').val());   // get value of field
    
    document.getElementById('nhil').value = gross * 0.025;
    var nhil = document.getElementById('nhil').value;

    document.getElementById('getf').value = gross * 0.025;
    var getf = document.getElementById('getf').value;

    document.getElementById('leviesincl').value = parseFloat(gross) + parseFloat(nhil) + parseFloat(getf);
    document.getElementById('vat').value = parseFloat(document.getElementById('leviesincl').value) * 0.125;
    document.getElementById('amount').value = parseFloat(document.getElementById('vat').value) + parseFloat(document.getElementById('leviesincl').value);
// add them and output it
});
  }

else if(stype=="CHBSALE"){
   document.getElementById("nhil").readOnly = false;
    document.getElementById("getf").readOnly = false;
    document.getElementById("leviesincl").readOnly = false;
    document.getElementById('vat').readOnly = false;
    document.getElementById('amount').readOnly = false;
    document.getElementById("grainvono").readOnly = false;
    document.getElementById("grainvono").required = false;

    $('input').keyup(function(){ // run anytime the value changes
    var gross  = Number($('#gross').val());   // get value of field
    
    document.getElementById('nhil').value = gross * 0.0;
    var nhil = document.getElementById('nhil').value;

    document.getElementById('getf').value = gross * 0.0;
    var getf = document.getElementById('getf').value;

    document.getElementById('leviesincl').value = parseFloat(gross) + parseFloat(nhil) + parseFloat(getf);
    document.getElementById('vat').value = parseFloat(document.getElementById('leviesincl').value) * 0.0;
    document.getElementById('amount').value = parseFloat(document.getElementById('vat').value) + parseFloat(document.getElementById('leviesincl').value);
    

    // document.getElementById('amount').value = parseFloat(gross);
// add them and output it
});
  }
}
</script>
    
  <script>
 document.getElementById("groupid").style.display = "none"; 
document.getElementById('groupid').value = Date.now() + Math.random();

</script>


  </body>
</html>

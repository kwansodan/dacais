<?php
//including the database connection file
include_once("config.php");
require __DIR__ . '/readerauth.php';
$variablee = $_SESSION['aic'];
//fetching data in descending order (lastest entry first)
//$result = mysql_query("SELECT * FROM users ORDER BY id DESC"); // mysql_query is deprecated
$result = mysqli_query($mysqli, "SELECT distinct mainclass FROM coa "); // using mysqli_query instead  SELECT * FROM Products WHERE Price BETWEEN 10 AND 20;


if (!$result) {
printf("Error: %s\n", mysqli_error($mysqli));
exit();

}
?>
<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<title>Create account</title>

</head>
<body>


<?php 
include 'header.php';
?>
<h1> Account Creation </h1>
<p> Query for available account numbers </p><br>
<form action="accountcreator.php" method="post">
<div class="form-row">
<div class="col-md-3 mb-3" >
<label for="validationDefault01">Account Type</label>
<select class="custom-select" name="acname">
<!-- <option selected>Select a main account class</option> -->
<?php
while($res = mysqli_fetch_array($result)) {  
echo "<option value='" . $res['mainclass'] . "'>" . $res['mainclass'] ."</option>";
}
?>
</select> 
</div>
</div>
<button class="btn btn-primary" type="submit" name="submit">Check</button>
</form>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/components/dropdown.min.js" integrity="sha512-8F/2JIwyPohlMVdqCmXt6A6YQ9X7MK1jHlwBJv2YeZndPs021083S2Z/mu7WZ5g0iTlGDYqelA9gQXGh1F0tUw==" crossorigin="anonymous"></script>

</body>
</html>

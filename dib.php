<?php 
include_once("config.php");
include_once("acfunctions.php");
$customersres = mysqli_query($mysqli, "SELECT * FROM coa WHERE accountno BETWEEN 1001 AND 1100");

?>

<?php
if(isset($_POST['submit'])) {
$bank = mysqli_real_escape_string($mysqli, $_POST['bank']);


$result = mysqli_query($mysqli, "SELECT gl2.id, gl2.dateo, gl1.groupid, gl2.acnumber, coa.accountname, gl2.description, gl2.amount
FROM
(Select groupid FROM gl WHERE acnumber = '".$bank."' and dateo=curdate()) gl1
LEFT JOIN 
(SELECT id, acnumber, description,dateo, groupid, amount FROM gl WHERE acnumber != '".$bank."' and dateo=curdate()) gl2
ON gl1.groupid = gl2.groupid
LEFT JOIN 
(SELECT accountno, accountname FROM coa) coa
ON gl2.acnumber = coa.accountno
ORDER BY gl2.id ASC");


if (!$result) {
    printf("Error: %s\n", mysqli_error($mysqli));
    exit();

}
}
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>DASHBOARD</title>
      <?php 
   include 'header.php';
?>
  </head>
  <body>
      <div class="container">

<form action="" method="post">
  <div class="form-row">
    
<div class="col-md-3 mb-3" id="cust" >
      <label for="validationDefault02">Select Bank Account</label>
      <input list="browser" name="bank" id="bank" class="form-control" required>
  <datalist id="browser">
        <?php
while($res = mysqli_fetch_array($customersres)) {  
echo "<option value='" . $res['accountno'] . "'>" . $res['accountname'] . "|" . $res['mainclass'] ."</option>";
}
?>
  </datalist>
    </div>

    
</div>
  <button class="btn btn-primary" type="submit" name="submit">Query</button>
  
</form><br><br>


        <div class="col-md-3 mb-3">
      
      <input type="text" class="form-control" id="searchbox" placeholder="Search..." onkeyup="myFunction()">
      
    </div>

  <table class="table table-sm" >
  <thead class="thead-dark">
    <tr>
      <th scope="col">Date</th>
      <th scope="col">Amount</th>
      <th scope="col">Class</th>
      <th scope="col">Description</th>
      <th scope="col">Balance</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody id="myTable">
     <?php 
    while($res = mysqli_fetch_array($result)) { 
      $dayman_set = 'dayman';
      $dayman_set = $dayman_set($res['amount']);
      $runningbal += $dayman_set[0];
      $show = $day_open['dayopen'] + $runningbal; 
        echo "<tr>";
        echo "<td>".$res['dateo']."</td>";
        echo "<td>".$dayman_set[1]."</td>";
        echo "<td>".$res['accountname']."</td>";
        echo "<td>".$res['description']."</td>";
        echo "<td>".number_format($show, 2, '.', ',')."</td>"; 
        echo "<td><a target='_blank' href=\"edit.php?id=$res[groupid]\">Edit</a></td>";
    }
    ?>
  </tbody>
</table>
    

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("#searchbox").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

  </body>
</html>
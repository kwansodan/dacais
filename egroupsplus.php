<?php

include_once("config.php");

?>
<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Favicon -->
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<title>JV</title>

</head>
<body>


<?php 
include 'header.php';
?>
<div class="container">
<h1> Add Employee Group</h1>
<form action="add.php" method="post" name="Journal" onsubmit="return(validate());">

<div class="form-row">
<div class="input-group input-group-sm mb-3">
  <span class="input-group-text" id="inputGroup-sizing-sm">Group name</span>
  <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
</div>


<div class="input-group input-group-sm mb-3">
  <span class="input-group-text" id="inputGroup-sizing-sm">Group purpose</span>
  <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
</div>
</div>


<div class="form-row">
<div class="col-md-4 mb-3">
<label for="exampleFormControlTextarea1">Description</label>
<textarea class="form-control" id="exampleFormControlTextarea1" name="description" rows="1" required="required"></textarea>
</div> 


<div class="col-md-4 mb-3" id="individ" >
<label for="validationDefault02">Ref.</label>
<input name="ref" id="ref" placeholder="Eg: cheque #, invoice #, etc." class="form-control" autocomplete="off" >
</div>
</div>

<div class="col-md-4 mb-3">
<input type="text" name="groupid" id="groupid" placeholder="guid" class="form-control" />
</div> 




<table  id="dynamic_field">
<thead>
<tr>
<th scope="col">Account</th>
<th scope="col">Amount GHS</th>
<th scope="col">Action</th>
</tr>
</thead>
<tbody>
<tr>

<td><input type="text" list="browsers" name="account[]" class="awams" placeholder="A/c #"  /></td>
<datalist id="browsers">
<?php
while($res = mysqli_fetch_array($result1)) {  
echo "<option value='" . $res['accountno'] . "'>" . $res['accountname'] . "|" . $res['mainclass'] ."</option>";
}
?>
</datalist>

<td><input type="text" name="amount[]" placeholder="0.00"  autocomplete="off" /></td>
<td><button type="button" name="add" id="add"  class="btn btn-outline-dark"><i class="fa fa-plus-square-o fa-fw" aria-hidden="true"></i></button></td>
</tr>

</tbody>
</table>

<br><br>

<button class="btn btn-outline-dark" type="submit" name="submit">Create group</button>
</form>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/components/dropdown.min.js" integrity="sha512-8F/2JIwyPohlMVdqCmXt6A6YQ9X7MK1jHlwBJv2YeZndPs021083S2Z/mu7WZ5g0iTlGDYqelA9gQXGh1F0tUw==" crossorigin="anonymous"></script>

</body>
</html>

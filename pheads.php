<?php 
include_once("config.php");
include_once("acfunctions.php");
$pheads = mysqli_query($mysqli, "SELECT distinct(name), description, type1, type2, freq, amount FROM payheads");

?>

<?php
if(isset($_POST['submit'])) {

  $length = count($_POST['fromamount']);
  $query= "INSERT INTO payheads(name, description, type1, type2, type3, freq, amount, comporflat, base, effectivedate, fromamount, toamount, rate, groupid) VALUES";
  for ($i = 0; $i < $length; $i++) {

$phname = mysqli_real_escape_string($mysqli, $_POST['phname']);
$phdescrip = mysqli_real_escape_string($mysqli, $_POST['phdescrip']);
$phtype1 = mysqli_real_escape_string($mysqli, $_POST['phtype1']);
$phtype2 = mysqli_real_escape_string($mysqli, $_POST['phtype2']);
$phtype3 = mysqli_real_escape_string($mysqli, $_POST['phtype3']);
$phfreq = mysqli_real_escape_string($mysqli, $_POST['phfreq']);
$phamount = mysqli_real_escape_string($mysqli, $_POST['phamount']);
$groupid = mysqli_real_escape_string($mysqli, $_POST['groupid']);
$comporflat = mysqli_real_escape_string($mysqli, $_POST['comporflat']);
$base = mysqli_real_escape_string($mysqli, $_POST['base']);
$effectivedate = mysqli_real_escape_string($mysqli, $_POST['effectivedate']);
$fromamount = mysqli_real_escape_string($mysqli, $_POST['fromamount'][$i]);
$toamount = mysqli_real_escape_string($mysqli, $_POST['toamount'][$i]);
$rate = mysqli_real_escape_string($mysqli, $_POST['rate'][$i]);


$query.= "('".$phname."','".$phdescrip."','". $phtype1."','". $phtype2."','". $phtype3."','".$phfreq."','". $phamount."','".$comporflat."','".$base."','".$effectivedate."','".$fromamount."','".$toamount."','".$rate."','".$groupid."'),";

}

$query = rtrim($query,",");
  if(!mysqli_query($mysqli, $query)){
  printf("Error: %s\n", mysqli_error($mysqli));}else{
  	$app_saldets=mysqli_query($mysqli, "ALTER TABLE salarydets ADD $phname varchar(500);");
    echo "<script type='text/javascript'> document.location = 'pheads.php'; </script>";
  exit();
  }
}


?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Pay heads</title>
      <?php 
   include 'header.php';
?>
  </head>
  <body>
      <div class="container">

<table class="table table-sm "> 
  <!-- <thead>
    <tr>
      <th scope="col"></th>
      <th scope="col"></th>
      <th scope="col"></th>
      <th scope="col"></th>
      <th scope="col"></th>
      <th scope="col"></th>
      <th scope="col"></th>
    </tr>
  </thead> -->
  <tbody>
    <tr>
 
  <td ><div class="col-md-4 mb-3"><input type="text" class="form-control" id="searchbox" placeholder="Search..."  onkeyup="myFunction()"></div></td>
   <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
       <td></td>
      <td></td>
      <td></td>
       <td></td>
      <td></td>
      <td></td>
  <td><button type="button" data-toggle="modal" data-target="#exampleModal"  class="btn btn-outline-dark"><i class="fa fa-plus-square-o fa-fw" aria-hidden="true"></i></button></td>   
</tr>
  </tbody>

</table>
    

  <table class="table table-sm table-hover" id="myTable">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Pay head</th>
      <th scope="col">Description</th>
      <th scope="col">Type 1</th>
      <th scope="col">Type 2</th>
      <th scope="col">Amount (fixed EP)</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
      
       <?php 
while($res = mysqli_fetch_array($pheads)) {
echo "<tr>";
echo "<th scope='row'>1</th>";
echo "<td>". $res['name']. "</td>";
echo "<td>". $res['description']. "</td>";
echo "<td>". $res['type1']. "</td>";
echo "<td>". $res['type2']. "</td>";
echo "<td>". $res['freq']. "</td>";
echo "<td>". $res['amount']. "</td>";
echo "<td><button type='button' name='view' id='view'  class='btn btn-outline-dark'><i class='fa fa-eye fa-fw' aria-hidden='true'></i></button></td>";
echo "<td><button type='button' name='update' id='update'  class='btn btn-outline-dark'><i class='fa fa-pencil-square-o fa-fw' aria-hidden='true'></i></button></td>";
echo "</tr>";
}
?>  
  </tbody>
</table>
    



<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Payhead</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="" method="post">
        <div class="col-sm-10">
        <input type="text" name="phname" class="form-control" placeholder="Payhead">
      </div>
      <div class="col-sm-10">
        <input type="text" name="phdescrip" class="form-control" placeholder="Description">
      </div>
      <div class="col-sm-10">
       <select class="form-control form-control-sm" name="phtype1">
  <option>Select PH type</option>
  <option>Deductions</option>
  <option>Additions</option>
</select>
      </div>
      <div class="col-sm-10">
       <select class="form-control form-control-sm" name="phtype2">
  <option>Select PH type1</option>
  <option>Other</option>
  <option>Basic</option>
  <option>Allowance</option>
  <option>Bonus</option>
  <option>SSNIT</option>
  <option>PAYE</option>
</select>
      </div>

      <div class="col-sm-10">
       <select class="form-control form-control-sm" id="phtype3" name="phtype3" onchange="determiner(this.value)">
  <option>Select PH type3</option>
  <option value="flat-constant">Flat - Constant</option>
  <option value="flat-variable">Flat - Variable</option>
  <option value="computed" >As computed</option>
</select>
      </div>

      <div class="col-sm-10">
       <select class="form-control form-control-sm" name="phfreq">
  <option>Daily</option>
  <option>Weekly</option>
  <option>Fortnightly</option>
  <option>Monthly</option>
  <option>Yearly</option>
</select>
      </div>

      <div class="camount col-sm-10">
        <input type="text" name="phamount" class="form-control" placeholder="camount">
      </div>

      <div class="col-md-4 mb-3">
    <input type="text" name="groupid" id="groupid" placeholder="guid" class="form-control" />
    </div> 

 <div class="basis col-sm-10">
       <select class="form-control form-control-sm" name="base">
  <option>Select Basis</option>
  <option>Basic</option>
  <option>Allowance</option>
  <option>Bonus</option>
  <option>SSNIT</option>
  <option>PAYE</option>
  <option>Total earnings</option>

</select>
      </div>
<div class="col-sm-10">
          <label for="validationDefault01">Effective from</label>
        <input type="date" id="amount" name="effectivedate" class="form-control">
      </div>
<div class="final">
       <table  id="dynamic_field">
  <thead>
    <tr>
      <th scope="col">From</th>
      <th scope="col">To </th>
      <th scope="col">Rate (%)</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      
      <td><input type="text"  name="fromamount[]" class="awams" placeholder="From amount"  /></td>
     <td><input type="text"  name="toamount[]" class="awams" placeholder="To amount"  /></td>
      <td><input type="text" name="rate[]" placeholder="%"  autocomplete="off" /></td>

      <td><button type="button" name="add" id="add"  class="btn btn-outline-dark"><i class="fa fa-plus-square-o fa-fw" aria-hidden="true"></i></button></td>
    </tr>
    
  </tbody>
  </table>
</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" name="submit" class="btn btn-primary">Save payhead</button>
      </div>
    </form>
    </div>
  </div>
</div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://use.fontawesome.com/ae1f260992.js"></script>
<script>
$(document).ready(function(){
  $("#searchbox").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

<script>


function determiner(phtype3){
  if(phtype3=="flat-constant"){

 $(".final").hide();
 $(".basis").hide();
 $(".camount").show();
;
  }

else if(phtype3=="flat-variable"){
  
 $(".final").hide();
 $(".basis").hide();
 $(".camount").hide();

}  
else if(phtype3=="computed"){
 $(".camount").hide();
  $(".final").show();

}  
  
}
</script>


  
<script>
$(document).ready(function(){
  var i=1;
  $('#add').click(function(){
    i++;
    $('#dynamic_field').append('<tr id="row'+i+'"><td><input type="text"  name="fromamount[]" class="awams" placeholder="From amount"  /></td><td><input type="text"  name="toamount[]" class="awams" placeholder="To amount"  /></td> <td><input type="text" name="rate[]" placeholder="%"  autocomplete="off" /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');
  });
  
  $(document).on('click', '.btn_remove', function(){
    var button_id = $(this).attr("id"); 
    $('#row'+button_id+'').remove();
  });
  
 
});
</script>
<script>
 document.getElementById("groupid").style.display = "none"; 
document.getElementById('groupid').value = Date.now() + Math.random();

</script>

  </body>
</html>
<?php
require __DIR__ . '/readerauth.php';
?>

<?php 
include_once("config.php");


if(isset($_POST['submit'])) {
$ac = mysqli_real_escape_string($mysqli, $_POST['acname']);

$result = mysqli_query($mysqli, "select distinct subclass from coa where mainclass = '".$ac."'");

}

if (!$result) {
    printf("Errror: %s\n", mysqli_error($mysqli));
    exit();

}

?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>AIS</title>
    <?php 
   include 'header.php';
?>
  </head>
  <body>
    
       <h1> Account Creation </h1>
<form action="addaccount.php" method="post">
  <div class="form-row">
       <div class="col-md-3 mb-3">
      <label for="validationDefault02">Ac name</label>
      <input type="text" class="form-control" name="acname" id="acname" autocomplete="off" required>
    </div>


    <div class="col-md-3 mb-3">
      <label for="validationDefault02">Account number</label>
      <input type="text" class="form-control" name="acnum" id="acnum" autocomplete="off" required>
    </div>

 

    <div class="col-md-3 mb-3">
      <label for="validationDefault02">subclass</label>
      <input type="text" list="browsers" class="form-control" name="subclass" id="subclass" autocomplete="off" required>
        <datalist id="browsers">
      <?php
      while($res = mysqli_fetch_array($result)) {  
      echo "<option value='" . $res['subclass'] . "'>" .  $res['subclass'] ."</option>";
      }
      ?>
      </datalist>



    </div>

    <div class="col-md-3 mb-3">
      <label for="validationDefault02">main class</label>
      <input type="text" class="form-control" name="mainclass" autocomplete="off" id="mainclass" <?php echo "value='".$ac."'";?>  required>
    </div>
    
    




  <button class="btn btn-primary" type="submit" name="submit">Create Account</button>
</form>  

  
  </div>
</div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>
</html>
